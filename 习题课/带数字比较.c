#include <stdio.h>
#include <string.h>

char stra[200] = {0}, strb[200] = {0};

int cmp(char *sa, char *sb)
{
    int numa[200] = {0}, numb[200] = {0}, i, j;
    int la, lb;
    la = strlen(sa);
    lb = strlen(sb);
    for (i = 0, j = la - 1; i < la; ++i, --j)
    {
        numa[j] = sa[i] - '0';
    }
    for (i = 0, j = lb - 1; i < lb; ++i, --j)
    {
        numb[j] = sb[i] - '0';
    }
    for (i = 199; i >= 0; --i)
    {
        if (numa[i] > numb[i])
        {
            return 1;
        }
        else if (numa[i] < numb[i])
        {
            return 0;
        }
    }
}

int main()
{
    int T;
    scanf("%d", &T);
    while (T--)
    {
        scanf("%s %s", stra, strb);
        if (stra[0] == '-' && strb[0] != '-')
        {
            printf("zd\n");
        }
        else if (stra[0] != '-' && strb[0] == '-')
        {
            printf("xx\n");
        }
        else if (stra[0] == '-')
        {
            if (cmp(stra + 1, strb + 1))
            {
                printf("zd\n");
            }
            else
            {
                printf("xx\n");
            }
        }
        else
        {
            if (cmp(stra, strb))
            {
                printf("xx\n");
            }
            else
            {
                printf("zd\n");
            }
        }
    }
}