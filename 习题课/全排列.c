#include <stdio.h>

int stack[12] = {0}, hass[12] = {0};
int top;

int notexist(int i)
{
    int j;
    for (j = 0; j < top; ++j)
    {
        if (stack[j] == i)
        {
            return 0;
        }
    }
    return 1;
}

void check(int n)
{
    int i;
    if (top == n)
    {
        // 输出
        for (i = 0; i < top; ++i)
        {
            printf("%d ", stack[i]);
        }
        putchar('\n');
    }
    else
    {
        for (i = 1; i <= n; ++i)
        {
            if (hass[i] == 0)
            {
                //压栈递归
                stack[top] = i;
                hass[i] = 1;
                top++;
                check(n);
                top--;
                hass[i] = 0;
            }
        }
    }
}

int main()
{
    int n;
    scanf("%d", &n);
    check(n);
}
