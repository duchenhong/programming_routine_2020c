#include <stdio.h>
#include <string.h>
#include <stdlib.h>

char A[110], B[110]; // 读入A,B两个大整数
int negA, negB; // 标记A, B分别是否为负数，是负数为1，非负为0

int cmpnum(const char *a, const char *b) { // 比较两个非负大整数
    int l1 = strlen(a), l2 = strlen(b);
    if (l1 != l2) return l1 - l2; // 长度不相等，长度大者数值更大
    else return strcmp(a, b); // 长度相等，高位大者数值更大（比字典序即可）
}

int main()
{
    int T; scanf("%d", &T); // T组输入数据
    while (T--) {
        int flg ;
        negA = negB = 0; // 这里一定要把负数标记初始化
        scanf("%s %s", A, B); 
        if (A[0] == '-') negA = 1;
        if (B[0] == '-') negB = 1;
        if (!negA && !negB) // 俩数都非负，直接比较绝对值大小
            flg = cmpnum(A, B);
        else if (negA && !negB) // A负，A<B
            flg = -1;
        else if (!negA && negB) // B负，A>B
            flg = 1;
        else flg = -cmpnum(A+1, B+1); // AB都负，大小关系为绝对值大小关系反过来
        if (flg < 0) puts("zd");
        else puts("xx");
    }


    return 0;
}
/* AUTHOR: dhy */