#include <stdio.h>
#include <stdlib.h>

#define isleap(y) ((y)%400==0||((y)%4==0&&(y)%100!=0))
const int cntDay[2][13] = {{0, 31, 28, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31},
                            {0, 31, 29, 31, 30, 31, 30, 31, 31, 30, 31, 30, 31}};
int dayOfYear(int y, int m, int d) {
    int leapflg = isleap(y);
    int cnt = 0;
    for (int i = 1; i < m; i++) cnt += cntDay[leapflg][i];
    cnt += d;
    return cnt;
}
#define nleap(y1,y2) (((y2) / 4 - (y1) / 4) - ((y2) / 100 - (y1) / 100) + ((y2) / 400 - (y1) / 400) + (!isleap((y2))) - (!isleap((y1))))
#define ydiff(y1,y2) (365*((y2)-(y1))+nleap((y1),(y2)))

int year, month, day;
int cu_year, cu_month, cu_day;
int main()
{
    while (scanf("%d%d%d", &year, &month, &day) != EOF) {
        scanf("%d%d%d", &cu_year, &cu_month, &cu_day);
        // 今年生日过去了否
        if (cu_month < month || (cu_month == month && cu_day <= day))
            year = cu_year;
        else year = cu_year + 1;
        if (month == 2 && day == 29)
            while (!isleap(year)) year++;
        int cnt1 = dayOfYear(cu_year, cu_month, cu_day);
        int cnt2 = dayOfYear(year, month, day);
        printf("%d\n", ydiff(cu_year, year) + (cnt2 - cnt1));
    }
    return 0;
}